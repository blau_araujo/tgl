# Trilha GNU/Linux 2024 

## Recursos

- [Site da Trilha](https://blauaraujo.com/tgl)
- [Playlist no Youtube](https://www.youtube.com/playlist?list=PLXoSGejyuQGpdRtaeeCZYItz__NY5_Usw)
- [Dúvidas e discussões](https://codeberg.org/blau_araujo/tgl/issues)
- [Wiki](https://codeberg.org/blau_araujo/tgl/wiki)

